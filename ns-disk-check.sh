#!/bin/sh

# call from external system with  shell /root/ns-disk-check.sh
#place file in /root directory
#recreate it after every update cause it's deleted by the update.

#Threshold of disk usage to report on
threshold=70   # 70 % is de max used space

# Get disk usage
diskUsage=`df -h | grep "/var$" | head -n 1 | awk {' print $5 '} | sed -n -e "s/%//p"`

# Check
if [ $diskUsage -gt $threshold ]; then
echo "more than $threshold % disk used"
else
echo "disk ok"
fi

